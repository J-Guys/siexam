﻿using MySql.Data.MySqlClient;

namespace RestfulLoadBalancer
{
    public class DBConnection
    {
        private static readonly string username = "jguys";
        private static readonly string password = "jguysatitagain";
        private static readonly string endport = "phonefabledb.catsvietenir.us-east-2.rds.amazonaws.com";
        private static readonly uint port = 3306;
        private static readonly string dbName = "SystemIntegration";

        public static string GetConnectionString()
        {
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
            builder.Server = endport;
            builder.UserID = username;
            builder.Password = password;
            builder.Port = port;
            builder.Database = dbName;

            return builder.ToString();
        }

        private static MySqlCommand cmd;
        private static MySqlConnection dbConn;

        public static string GetUser(string username)
        {
            using (dbConn = new MySqlConnection(GetConnectionString()))
            {
                dbConn.Open();

                string sqlQuery = "SELECT `Username`, `Password` FROM `Users` WHERE Username = '" + username + "'";
                cmd = new MySqlCommand(sqlQuery, dbConn);
                int result = cmd.ExecuteNonQuery();
                string fullData = "";

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();

                    if (reader.HasRows)
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            fullData += reader[i];
                            fullData += "|";
                        }

                        dbConn.Close();
                        return fullData;
                    }
                    else
                    {
                        dbConn.Close();
                        return "None";
                    }
                }
            }
        }
    }
}