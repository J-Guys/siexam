﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestfulLoadBalancer.Controllers
{
    public class ReadRequestController : ApiController
    {
        // GET: api/ReadRequest
        public IEnumerable<string> Get()
        {
            return new string[] { "vagjk", "value2" };
        }

        // GET: api/ReadRequest/5
        public string Get(string id)
        {
            string result = DBConnection.GetUser(id);

            return result;
        }

        // POST: api/ReadRequest
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ReadRequest/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ReadRequest/5
        public void Delete(int id)
        {
        }
    }
}
