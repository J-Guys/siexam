﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace ConnectionData
{
    public static class Connections
    {
        public static ConnectionFactory RabbitFactoryChat()
        {
            return new ConnectionFactory() { UserName = "chatter", Password = "chat121", HostName = "10.0.0.100", Port = 5672, VirtualHost = "/" };
        }

        public static ConnectionFactory RabbitFactoryLocal()
        {
            return new ConnectionFactory() { HostName = "localhost" };
        }
    }
}
