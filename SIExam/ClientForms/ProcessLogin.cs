﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ClientForms
{
    public static class ProcessLogin
    {
        static string message = "";
        const int PORT_NO = 5000;
        const string SERVER_IP = "10.0.0.101";

        static bool isConnected;
        static StreamReader sReader;
        static StreamWriter sWriter;
        static TcpClient client;


        public static void AttemptLogin(string username, string password)
        {
            message = "L|" + username + "|" + password;
            Client(message);
        }

        public static void AttemptCreateAccount(string username, string password, string email)
        {
            message = "C|" + username + "|" + password + "|" + email;
            Client(message);
        }

        public static void Client(string message)
        {
            //---data to send to the server---
            string textToSend = DateTime.Now.ToString();

            //---create a TCPClient object at the IP and port no.---
            client = new TcpClient(SERVER_IP, PORT_NO);

            HandleCommunication(message);

            //NetworkStream nwStream = client.GetStream();
            //byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(message + " - " + textToSend);

            ////---send the text---
            //nwStream.Write(bytesToSend, 0, bytesToSend.Length);
            
            ////---read back the text---
            //byte[] bytesToRead = new byte[client.ReceiveBufferSize];
            //int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
            //Console.WriteLine("Received : " + Encoding.ASCII.GetString(bytesToRead, 0, bytesRead));
            //Console.ReadLine();
            //client.Close();
        }

        static void HandleCommunication(string message)
        {
            sReader = new StreamReader(client.GetStream(), Encoding.ASCII);
            sWriter = new StreamWriter(client.GetStream(), Encoding.ASCII);
            isConnected = true;
            String sData = null;
            while (isConnected)
            {
                Console.Write("> ");
                sData = message;
                // write data and make sure to flush, or the buffer will continue to 
                // grow, and your data might not be sent when you want it, and will
                // only be sent once the buffer is filled.
                sWriter.WriteLine(sData);
                sWriter.Flush();
                // if you want to receive anything
                // String sDataIncomming = _sReader.ReadLine();
            }
        }
    }
}
