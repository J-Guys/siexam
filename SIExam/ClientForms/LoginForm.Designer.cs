﻿namespace ClientForms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsernameTextbox = new System.Windows.Forms.TextBox();
            this.PasswordTextbox = new System.Windows.Forms.TextBox();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.LoginButton = new System.Windows.Forms.Button();
            this.CreateAccountButton = new System.Windows.Forms.Button();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.CreatePanel = new System.Windows.Forms.Panel();
            this.CreateBackButton = new System.Windows.Forms.Button();
            this.CreateErrorLabel = new System.Windows.Forms.Label();
            this.CreateEmailLabel = new System.Windows.Forms.Label();
            this.CreateEmailTextbox = new System.Windows.Forms.TextBox();
            this.CreateConfirmPasswordLabel = new System.Windows.Forms.Label();
            this.CreateConfirmPasswordTextbox = new System.Windows.Forms.TextBox();
            this.CreateUsernameLabel = new System.Windows.Forms.Label();
            this.CreateCreateButton = new System.Windows.Forms.Button();
            this.CreatePasswordLabel = new System.Windows.Forms.Label();
            this.CreatePasswordTextbox = new System.Windows.Forms.TextBox();
            this.CreateUsernameTextbox = new System.Windows.Forms.TextBox();
            this.LoginPanel = new System.Windows.Forms.Panel();
            this.LoginErrorLabel = new System.Windows.Forms.Label();
            this.CreatePanel.SuspendLayout();
            this.LoginPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // UsernameTextbox
            // 
            this.UsernameTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UsernameTextbox.Location = new System.Drawing.Point(50, 91);
            this.UsernameTextbox.Name = "UsernameTextbox";
            this.UsernameTextbox.Size = new System.Drawing.Size(200, 20);
            this.UsernameTextbox.TabIndex = 6;
            this.UsernameTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.UsernameTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UsernameTextbox_KeyDown);
            // 
            // PasswordTextbox
            // 
            this.PasswordTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PasswordTextbox.Location = new System.Drawing.Point(50, 181);
            this.PasswordTextbox.Name = "PasswordTextbox";
            this.PasswordTextbox.PasswordChar = '*';
            this.PasswordTextbox.Size = new System.Drawing.Size(200, 20);
            this.PasswordTextbox.TabIndex = 7;
            this.PasswordTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PasswordTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PasswordTextbox_KeyDown);
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordLabel.Location = new System.Drawing.Point(98, 152);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(108, 26);
            this.PasswordLabel.TabIndex = 9;
            this.PasswordLabel.Text = "Password";
            // 
            // LoginButton
            // 
            this.LoginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginButton.Location = new System.Drawing.Point(50, 239);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(200, 40);
            this.LoginButton.TabIndex = 10;
            this.LoginButton.Text = "Login";
            this.LoginButton.UseVisualStyleBackColor = true;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // CreateAccountButton
            // 
            this.CreateAccountButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateAccountButton.Location = new System.Drawing.Point(50, 345);
            this.CreateAccountButton.Name = "CreateAccountButton";
            this.CreateAccountButton.Size = new System.Drawing.Size(200, 40);
            this.CreateAccountButton.TabIndex = 11;
            this.CreateAccountButton.Text = "Create New User";
            this.CreateAccountButton.UseVisualStyleBackColor = true;
            this.CreateAccountButton.Click += new System.EventHandler(this.CreateAccountButton_Click);
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameLabel.Location = new System.Drawing.Point(98, 62);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(113, 26);
            this.UsernameLabel.TabIndex = 8;
            this.UsernameLabel.Text = "Username";
            // 
            // CreatePanel
            // 
            this.CreatePanel.Controls.Add(this.CreateBackButton);
            this.CreatePanel.Controls.Add(this.CreateErrorLabel);
            this.CreatePanel.Controls.Add(this.CreateEmailLabel);
            this.CreatePanel.Controls.Add(this.CreateEmailTextbox);
            this.CreatePanel.Controls.Add(this.CreateConfirmPasswordLabel);
            this.CreatePanel.Controls.Add(this.CreateConfirmPasswordTextbox);
            this.CreatePanel.Controls.Add(this.CreateUsernameLabel);
            this.CreatePanel.Controls.Add(this.CreateCreateButton);
            this.CreatePanel.Controls.Add(this.CreatePasswordLabel);
            this.CreatePanel.Controls.Add(this.CreatePasswordTextbox);
            this.CreatePanel.Controls.Add(this.CreateUsernameTextbox);
            this.CreatePanel.Location = new System.Drawing.Point(2, 3);
            this.CreatePanel.Name = "CreatePanel";
            this.CreatePanel.Size = new System.Drawing.Size(479, 455);
            this.CreatePanel.TabIndex = 12;
            this.CreatePanel.Visible = false;
            // 
            // CreateBackButton
            // 
            this.CreateBackButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CreateBackButton.FlatAppearance.BorderSize = 0;
            this.CreateBackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateBackButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateBackButton.Image = global::ClientForms.Properties.Resources.arrowLeft;
            this.CreateBackButton.Location = new System.Drawing.Point(2, 3);
            this.CreateBackButton.Name = "CreateBackButton";
            this.CreateBackButton.Size = new System.Drawing.Size(63, 40);
            this.CreateBackButton.TabIndex = 16;
            this.CreateBackButton.UseVisualStyleBackColor = true;
            this.CreateBackButton.Click += new System.EventHandler(this.CreateBackButton_Click);
            // 
            // CreateErrorLabel
            // 
            this.CreateErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.CreateErrorLabel.Location = new System.Drawing.Point(89, 17);
            this.CreateErrorLabel.Name = "CreateErrorLabel";
            this.CreateErrorLabel.Size = new System.Drawing.Size(300, 54);
            this.CreateErrorLabel.TabIndex = 13;
            this.CreateErrorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CreateEmailLabel
            // 
            this.CreateEmailLabel.AutoSize = true;
            this.CreateEmailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateEmailLabel.Location = new System.Drawing.Point(203, 314);
            this.CreateEmailLabel.Name = "CreateEmailLabel";
            this.CreateEmailLabel.Size = new System.Drawing.Size(68, 26);
            this.CreateEmailLabel.TabIndex = 15;
            this.CreateEmailLabel.Text = "Email";
            // 
            // CreateEmailTextbox
            // 
            this.CreateEmailTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CreateEmailTextbox.Location = new System.Drawing.Point(139, 340);
            this.CreateEmailTextbox.Name = "CreateEmailTextbox";
            this.CreateEmailTextbox.Size = new System.Drawing.Size(200, 20);
            this.CreateEmailTextbox.TabIndex = 14;
            this.CreateEmailTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CreateEmailTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CreateEmailTextbox_KeyDown);
            // 
            // CreateConfirmPasswordLabel
            // 
            this.CreateConfirmPasswordLabel.AutoSize = true;
            this.CreateConfirmPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateConfirmPasswordLabel.Location = new System.Drawing.Point(144, 231);
            this.CreateConfirmPasswordLabel.Name = "CreateConfirmPasswordLabel";
            this.CreateConfirmPasswordLabel.Size = new System.Drawing.Size(191, 26);
            this.CreateConfirmPasswordLabel.TabIndex = 13;
            this.CreateConfirmPasswordLabel.Text = "Confirm Password";
            // 
            // CreateConfirmPasswordTextbox
            // 
            this.CreateConfirmPasswordTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CreateConfirmPasswordTextbox.Location = new System.Drawing.Point(139, 260);
            this.CreateConfirmPasswordTextbox.Name = "CreateConfirmPasswordTextbox";
            this.CreateConfirmPasswordTextbox.PasswordChar = '*';
            this.CreateConfirmPasswordTextbox.Size = new System.Drawing.Size(200, 20);
            this.CreateConfirmPasswordTextbox.TabIndex = 12;
            this.CreateConfirmPasswordTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CreateConfirmPasswordTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CreateConfirmPasswordTextbox_KeyDown);
            // 
            // CreateUsernameLabel
            // 
            this.CreateUsernameLabel.AutoSize = true;
            this.CreateUsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateUsernameLabel.Location = new System.Drawing.Point(179, 71);
            this.CreateUsernameLabel.Name = "CreateUsernameLabel";
            this.CreateUsernameLabel.Size = new System.Drawing.Size(113, 26);
            this.CreateUsernameLabel.TabIndex = 8;
            this.CreateUsernameLabel.Text = "Username";
            // 
            // CreateCreateButton
            // 
            this.CreateCreateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateCreateButton.Location = new System.Drawing.Point(139, 389);
            this.CreateCreateButton.Name = "CreateCreateButton";
            this.CreateCreateButton.Size = new System.Drawing.Size(200, 40);
            this.CreateCreateButton.TabIndex = 11;
            this.CreateCreateButton.Text = "Create!";
            this.CreateCreateButton.UseVisualStyleBackColor = true;
            this.CreateCreateButton.Click += new System.EventHandler(this.CreateCreateButton_Click);
            // 
            // CreatePasswordLabel
            // 
            this.CreatePasswordLabel.AutoSize = true;
            this.CreatePasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatePasswordLabel.Location = new System.Drawing.Point(184, 151);
            this.CreatePasswordLabel.Name = "CreatePasswordLabel";
            this.CreatePasswordLabel.Size = new System.Drawing.Size(108, 26);
            this.CreatePasswordLabel.TabIndex = 9;
            this.CreatePasswordLabel.Text = "Password";
            // 
            // CreatePasswordTextbox
            // 
            this.CreatePasswordTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CreatePasswordTextbox.Location = new System.Drawing.Point(139, 180);
            this.CreatePasswordTextbox.Name = "CreatePasswordTextbox";
            this.CreatePasswordTextbox.PasswordChar = '*';
            this.CreatePasswordTextbox.Size = new System.Drawing.Size(200, 20);
            this.CreatePasswordTextbox.TabIndex = 7;
            this.CreatePasswordTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CreatePasswordTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CreatePasswordTextbox_KeyDown);
            // 
            // CreateUsernameTextbox
            // 
            this.CreateUsernameTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CreateUsernameTextbox.Location = new System.Drawing.Point(139, 100);
            this.CreateUsernameTextbox.Name = "CreateUsernameTextbox";
            this.CreateUsernameTextbox.Size = new System.Drawing.Size(200, 20);
            this.CreateUsernameTextbox.TabIndex = 6;
            this.CreateUsernameTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CreateUsernameTextbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CreateUsernameTextbox_KeyDown);
            // 
            // LoginPanel
            // 
            this.LoginPanel.Controls.Add(this.LoginErrorLabel);
            this.LoginPanel.Controls.Add(this.UsernameLabel);
            this.LoginPanel.Controls.Add(this.CreateAccountButton);
            this.LoginPanel.Controls.Add(this.LoginButton);
            this.LoginPanel.Controls.Add(this.PasswordLabel);
            this.LoginPanel.Controls.Add(this.PasswordTextbox);
            this.LoginPanel.Controls.Add(this.UsernameTextbox);
            this.LoginPanel.Location = new System.Drawing.Point(82, 12);
            this.LoginPanel.Name = "LoginPanel";
            this.LoginPanel.Size = new System.Drawing.Size(301, 437);
            this.LoginPanel.TabIndex = 6;
            // 
            // LoginErrorLabel
            // 
            this.LoginErrorLabel.AutoSize = true;
            this.LoginErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoginErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.LoginErrorLabel.Location = new System.Drawing.Point(3, 20);
            this.LoginErrorLabel.Name = "LoginErrorLabel";
            this.LoginErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.LoginErrorLabel.TabIndex = 12;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.CreatePanel);
            this.Controls.Add(this.LoginPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.CreatePanel.ResumeLayout(false);
            this.CreatePanel.PerformLayout();
            this.LoginPanel.ResumeLayout(false);
            this.LoginPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox UsernameTextbox;
        private System.Windows.Forms.TextBox PasswordTextbox;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Button LoginButton;
        private System.Windows.Forms.Button CreateAccountButton;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Panel CreatePanel;
        private System.Windows.Forms.Button CreateBackButton;
        private System.Windows.Forms.Label CreateEmailLabel;
        private System.Windows.Forms.TextBox CreateEmailTextbox;
        private System.Windows.Forms.Label CreateConfirmPasswordLabel;
        private System.Windows.Forms.TextBox CreateConfirmPasswordTextbox;
        private System.Windows.Forms.Label CreateUsernameLabel;
        private System.Windows.Forms.Button CreateCreateButton;
        private System.Windows.Forms.Label CreatePasswordLabel;
        private System.Windows.Forms.TextBox CreatePasswordTextbox;
        private System.Windows.Forms.TextBox CreateUsernameTextbox;
        private System.Windows.Forms.Panel LoginPanel;
        private System.Windows.Forms.Label LoginErrorLabel;
        private System.Windows.Forms.Label CreateErrorLabel;
    }
}