﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Windows.Forms;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ClientForms
{
    public class RabbitSimpleExample
    {
        ConnectionFactory factory;

        public void StartUp(bool local)
        {
            if (!local)
            {
                factory = new ConnectionFactory() { UserName = "chatter", Password = "chat121", HostName = "10.0.0.100", Port = 5672, VirtualHost = "/" };
            }
            else
            {
                factory = new ConnectionFactory() { HostName = "localhost" };
            }
        }
        public void SendMessage(string message)
        {
            if (message.StartsWith("@"))
            {
                SendPrivate(message);
            }
            else
            {
                SendGlobal(message);
            }
        }

        public void SendGlobal(string message)
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.QueueDeclare(
                queue: "chatMessageQueue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var body = Encoding.UTF8.GetBytes(User.Username + ": " + message);

            channel.BasicPublish(
                exchange: "",
                routingKey: "chatMessageQueue",
                basicProperties: null,
                body: body);
        }

        public void SendPrivate(string message)
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            message += "|" + User.Username;

            channel.QueueDeclare(
                queue: "chatMessagePrivateQueue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(
                exchange: "",
                routingKey: "chatMessagePrivateQueue",
                basicProperties: null,
                body: body);
        }

        public void RecieveGlobal(RichTextBox text)
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.ExchangeDeclare(exchange: "chat", type: ExchangeType.Fanout);

            var queueName = channel.QueueDeclare().QueueName;

            channel.QueueBind(
                queue: queueName,
                exchange: "chat",
                routingKey: "");

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                text.Invoke(new MethodInvoker(delegate { text.Text += message + "\n"; }));
            };

            channel.BasicConsume(
                queue: queueName,
                autoAck: true,
                consumer: consumer);
        }
        public void RecievePrivate(RichTextBox text)
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            var args = new Dictionary<string, object>();
            args.Add("x-expires", 60000);

            var queueName = channel.QueueDeclare(
                queue: User.Username,
                durable: false,
                exclusive: false,
                autoDelete: true,
                arguments: args).QueueName;

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                text.Invoke(new MethodInvoker(delegate { text.Text += message + "\n"; }));
            };

            channel.BasicConsume(
                queue: queueName,
                autoAck: true,
                consumer: consumer);
        }
    }
}
