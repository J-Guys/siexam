﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientForms
{
    public partial class LoginForm : Form
    {
        Form1 comsForm;
        public LoginForm()
        {
            InitializeComponent();
        }

        private void CreateAccountButton_Click(object sender, EventArgs e)
        {
            ResetLoginInfo();
            CreatePanel.Visible = true;
            LoginPanel.Visible = false;
        }

        private void CreateBackButton_Click(object sender, EventArgs e)
        {
            ResetCreateInfo();
            CreatePanel.Visible = false;
            LoginPanel.Visible = true;
        }

        private void UsernameTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Login();
                e.SuppressKeyPress = true;
            }
        }

        private void PasswordTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Login();
                e.SuppressKeyPress = true;
            }
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void CreateUsernameTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CreateNewUser();
                e.SuppressKeyPress = true;
            }
        }

        private void CreatePasswordTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CreateNewUser();
                e.SuppressKeyPress = true;
            }
        }

        private void CreateConfirmPasswordTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CreateNewUser();
                e.SuppressKeyPress = true;
            }
        }

        private void CreateEmailTextbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CreateNewUser();
                e.SuppressKeyPress = true;
            }
        }

        private void CreateCreateButton_Click(object sender, EventArgs e)
        {
            CreateNewUser();
        }

        private void Login()
        {
            var username = UsernameTextbox.Text;
            var password = PasswordTextbox.Text;

            if (username == "" || password == "")
            {
                LoginErrorLabel.Text = "Please fill in a valid username and password";
            }
            else
            {
                LoginErrorLabel.Text = "";
                string answer = RestfulClientPost.Post(username, password);
                answer = RemoveSpecialCharacters(answer);
                
                if(answer == "Allowed")
                {
                    User.Username = username;
                    this.Hide();
                    comsForm.FormClosed += (s, args) => this.Close();
                    comsForm.Show();
                }
                else if(answer == "None")
                {
                    LoginErrorLabel.Text = "No user with that name exists.";
                }
                else if(answer == "WrongPW")
                {
                    LoginErrorLabel.Text = "Password didn't match username.";
                }
                else
                {
                    LoginErrorLabel.Text = "ERROR ERROR IF THIS HAPPENS ALL IS LOST";
                }

            }
        }

        private void CreateNewUser()
        {
            var username = CreateUsernameTextbox.Text;
            var password = CreatePasswordTextbox.Text;
            var confirmPassword = CreateConfirmPasswordTextbox.Text;
            var email = CreateEmailTextbox.Text;

            if (username == "" || password == "" || confirmPassword == "" || email == "")
            {
                CreateErrorLabel.Text = "Please fill out all the information below";
            }
            else
            {
                if(password != confirmPassword)
                {
                    CreateErrorLabel.Text = "The entered passwords do not match";
                }
                else
                {
                    CreateErrorLabel.Text = "";
                    ProcessLogin.AttemptCreateAccount(username, password, email);              
                }            
            }
        }

        private void ResetLoginInfo()
        {
            LoginErrorLabel.Text = "";
            UsernameTextbox.Text = "";
            PasswordTextbox.Text = "";
        }

        private void ResetCreateInfo()
        {
            CreateErrorLabel.Text = "";
            CreateUsernameTextbox.Text = "";
            CreatePasswordTextbox.Text = "";
            CreateConfirmPasswordTextbox.Text = "";
            CreateEmailTextbox.Text = "";
        }

        public string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == '|')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            comsForm = new Form1();
        }
    }
}
