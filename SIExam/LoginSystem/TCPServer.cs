﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace LoginSystem
{
    public static class TCPServer
    {
        const int PORT_NO = 5000;
        const string SERVER_IP = "10.0.0.101";
        static bool _isRunning;

        public static void Server()
        {
            //---listen at the specified IP and port no.---
            IPAddress localAdd = IPAddress.Parse(SERVER_IP);
            TcpListener listener = new TcpListener(localAdd, PORT_NO);
            Console.WriteLine("Listening...");
            listener.Start();
            _isRunning = true;

            while (_isRunning)
            {
                //---incoming client connected---
                TcpClient client = listener.AcceptTcpClient();

                //Create a thread to handle communication
                Thread t = new Thread(new ParameterizedThreadStart(HandleClient));
                t.Start(client);


                ////---get the incoming data through a network stream---
                //NetworkStream nwStream = client.GetStream();
                //byte[] buffer = new byte[client.ReceiveBufferSize];

                ////---read incoming stream---
                //int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                ////---convert the data received into a string---
                //string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                //Console.WriteLine("Recieved: " + dataReceived);

                //MQLoginSender.Send(dataReceived);

                ////---write back the text to the client---
                //nwStream.Write(buffer, 0, bytesRead);
                //client.Close();
            }
            //listener.Stop();
            //Console.ReadLine();
        }

        public static void HandleClient(object obj)
        {
            TcpClient client = (TcpClient)obj;
 
            // sets two streams
            StreamWriter sWriter = new StreamWriter(client.GetStream(), Encoding.ASCII);
            StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII);

            // you could use the NetworkStream to read and write, 
            // but there is no forcing flush, even when requested
            bool bClientConnected = true;
            string sData = null;

            while (bClientConnected)
            {
                // reads from stream
                sData = sReader.ReadLine();
                // shows content on the console.
                Console.WriteLine("Client > " + sData);
            }
        }
    }
}
