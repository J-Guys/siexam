﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestfulLogin.Controllers
{
    public class LoginController : ApiController
    {
        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Login
        public string Post([FromBody]string value)
        {
            string username = value.Split('|')[0];
            string password = value.Split('|')[1];

            string answer = RestfulLoginGet.Get(username);

            string a = RestfulLoginGet.RemoveSpecialCharacters(answer);

            if (a == "None")
                return a;
            else if (a.Split('|')[1] == password)
                return "Allowed";
            else
                return "WrongPW";
        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {
        }
    }
}
