﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSystem
{
    public class SimpleReceive
    {
        ConnectionFactory factory;
        public void StartUp(bool local)
        {
            if (!local)
            {
                factory = new ConnectionFactory() { UserName = "chatter", Password = "chat121", HostName = "10.0.0.100", Port = 5672, VirtualHost = "/" };
            }
            else
            {
                factory = new ConnectionFactory() { HostName = "localhost" };
            }
        }
        public void RecieveGlobalMessages()
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            //var chatchannel = connection.CreateModel();

            channel.QueueDeclare(
                queue: "chatMessageQueue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                //var body = ea.Body.ToArray();
                //var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("Received: " + Encoding.UTF8.GetString(ea.Body.ToArray()));
                channel.BasicPublish(
                    exchange: "chat",
                    routingKey: "",
                    basicProperties: null,
                    body: ea.Body);
            };

            channel.BasicConsume(
                queue: "chatMessageQueue",
                autoAck: true,
                consumer: consumer);
        }

        public void RecievePrivateMessages()
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.QueueDeclare(
                queue: "chatMessagePrivateQueue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                string receivingUser = message.Split(' ')[0].Substring(1);
                string sendingUser = message.Split('|')[1];

                string receivingStart = "From " + sendingUser + ":";
                string sendingStart = "To " + receivingUser + ":";

                string fullMessage = message.Substring(receivingUser.Length + 1, message.Length - receivingUser.Length - sendingUser.Length - 2);

                var userChannel = connection.CreateModel();
                var userChannel2 = connection.CreateModel();

                var args = new Dictionary<string, object>();
                args.Add("x-expires", 60000);

                userChannel.QueueDeclare(
                    queue: receivingUser,
                    durable: false,
                    exclusive: false,
                    autoDelete: true,
                    arguments: args);

                userChannel2.QueueDeclare(
                    queue: sendingUser,
                    durable: false,
                    exclusive: false,
                    autoDelete: true,
                    arguments: args);

                Console.WriteLine("Received private: " + receivingUser + " : " + sendingUser + " : " + fullMessage);
                userChannel.BasicPublish(
                    exchange: "",
                    routingKey: receivingUser,
                    basicProperties: null,
                    body: Encoding.UTF8.GetBytes(receivingStart + fullMessage));

                userChannel2.BasicPublish(
                    exchange: "",
                    routingKey: sendingUser,
                    basicProperties: null,
                    body: Encoding.UTF8.GetBytes(sendingStart + fullMessage));
            };

            channel.BasicConsume(
                queue: "chatMessagePrivateQueue",
                autoAck: true,
                consumer: consumer);
        }

        public void PublishGlobal()
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.ExchangeDeclare(
                exchange: "chat", 
                type: ExchangeType.Fanout);

            var body = Encoding.UTF8.GetBytes("message");
            channel.BasicPublish(
                exchange: "chat",
                routingKey: "",
                basicProperties: null,
                body: body);
        }

        public void CreateGroupRPC()
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.QueueDeclare(
                queue: "create_group_rpc",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            channel.BasicQos(0, 1, false);

            var consumer = new EventingBasicConsumer(channel);

            channel.BasicConsume(
                queue: "rpc_queue",
                autoAck: false,
                consumer: consumer);

            consumer.Received += (model, ea) =>
            {
                string response = "groupCreated";

                var body = ea.Body.ToArray();
                var props = ea.BasicProperties;
                var replyProps = channel.CreateBasicProperties();
                replyProps.CorrelationId = props.CorrelationId;

                var message = Encoding.UTF8.GetString(body);
                var responseBytes = Encoding.UTF8.GetBytes(response);

                channel.BasicPublish(
                    exchange: "", 
                    routingKey: props.ReplyTo,
                    basicProperties: replyProps, 
                    body: responseBytes);

                channel.BasicAck(
                    deliveryTag: ea.DeliveryTag,
                    multiple: false);
            };
        }

        private void CreateGroup(string name)
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();

            channel.QueueDeclare(
                queue: name + "queue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            channel.ExchangeDeclare(
                exchange: name,
                type: ExchangeType.Fanout);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                //var body = ea.Body.ToArray();
                //var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("Received: " + Encoding.UTF8.GetString(ea.Body.ToArray()));
                channel.BasicPublish(
                    exchange: name,
                    routingKey: "",
                    basicProperties: null,
                    body: ea.Body);
            };

            channel.BasicConsume(
                queue: name + "queue",
                autoAck: true,
                consumer: consumer);
        }

        ////public void recieve()
        ////{
        ////    var factory = new ConnectionFactory() { HostName = "localhost" };
        ////    var connection = factory.CreateConnection();
        ////    var channel = connection.CreateModel();
        ////    var chatchannel = connection.CreateModel();

        ////    channel.QueueDeclare(queue: "testQueue", durable: false, exclusive: false, autoDelete: false, arguments: null);
        ////    chatchannel.ExchangeDeclare(exchange: "chat", type: ExchangeType.Fanout);

        ////    var consumer = new EventingBasicConsumer(channel);
        ////    consumer.Received += (model, ea) =>
        ////    {
        ////        //var body = ea.Body.ToArray();
        ////        //var message = Encoding.UTF8.GetString(body);
        ////        //Console.WriteLine("Received: " + message);
        ////        channel.BasicPublish(
        ////            exchange: "chat",
        ////            routingKey: "",
        ////            basicProperties: null,
        ////            body: ea.Body);
        ////    };

        ////    channel.BasicConsume(queue: "testQueue", autoAck: true, consumer: consumer);
        ////}

        ////public void publish()
        ////{
        ////    var factory = new ConnectionFactory() { HostName = "localhost" };

        ////    var connection = factory.CreateConnection();
        ////    var channel = connection.CreateModel();

        ////    channel.ExchangeDeclare(exchange: "chat", type: ExchangeType.Fanout);

        ////    var body = Encoding.UTF8.GetBytes("s");
        ////    channel.BasicPublish(
        ////        exchange: "chat",
        ////        routingKey: "",
        ////        basicProperties: null,
        ////        body: body);
        ////}

        ////public void recieve2()
        ////{
        ////    var factory = new ConnectionFactory() { UserName = "other", Password = "other123", HostName = "10.0.0.100", Port = 5672, VirtualHost = "/" };
        ////    var connection = factory.CreateConnection();
        ////    var channel = connection.CreateModel();
        ////    var chatchannel = connection.CreateModel();

        ////    channel.QueueDeclare(queue: "testQueue", durable: false, exclusive: false, autoDelete: false, arguments: null);
        ////    chatchannel.ExchangeDeclare(exchange: "chat", type: ExchangeType.Fanout);

        ////    var consumer = new EventingBasicConsumer(channel);
        ////    consumer.Received += (model, ea) =>
        ////    {
        ////        //var body = ea.Body.ToArray();
        ////        //var message = Encoding.UTF8.GetString(body);
        ////        Console.WriteLine("Received: " + Encoding.UTF8.GetString(ea.Body.ToArray()));
        ////        channel.BasicPublish(
        ////            exchange: "chat",
        ////            routingKey: "",
        ////            basicProperties: null,
        ////            body: ea.Body);
        ////    };

        ////    channel.BasicConsume(queue: "testQueue", autoAck: true, consumer: consumer);
        ////}

        ////public void publish2()
        ////{
        ////    var factory = new ConnectionFactory() { UserName = "other", Password = "other123", HostName = "10.0.0.100", Port = 5672, VirtualHost = "/" };

        ////    var connection = factory.CreateConnection();
        ////    var channel = connection.CreateModel();

        ////    channel.ExchangeDeclare(exchange: "chat", type: ExchangeType.Fanout);

        ////    var body = Encoding.UTF8.GetBytes("s");
        ////    channel.BasicPublish(
        ////        exchange: "chat",
        ////        routingKey: "",
        ////        basicProperties: null,
        ////        body: body);
        ////}
    }
}
