﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleReceive sr = new SimpleReceive();
            sr.StartUp(false);
            sr.RecieveGlobalMessages();
            sr.RecievePrivateMessages();
            //sr.publish();
            Console.ReadKey();
        }
    }
}
